import React from 'react';
import { isEmpty } from 'lodash';
import TaskList from './Components/TaskList/TaskList';

const Home = () => {
  const [todoList, setTodoList] = React.useState([]);
  const [inputtedTask, setInputtedTask] = React.useState('');
  const [idCounter, setIdCounter] = React.useState(todoList.length);
  
  const handleOnChangeInput = (event: any) => {
    setInputtedTask(event.target.value);
  };

  const handleOnClickAddButton = () => {
    const newData = {
      id: idCounter, 
      task: inputtedTask
    };
    setTodoList([...todoList, newData]);
    setIdCounter(idCounter + 1);
  };

  return (
    <div className="mainContainer">
    <h1 className="todoListTitle">TODO LIST</h1>
    <div className="inputContainer">
      <input
        type="text"
        placeholder=" add Task... "
        onChange={(e) => handleOnChangeInput(e)}
        className="inputText"
      />
      <input className="addTaskButton" value="Add Task" type="button" onClick={() => {handleOnClickAddButton()}}/>
    </div>
    <div>
      <ul className="gridList">
        {
          !isEmpty(todoList) ? 
          <>
          {
            todoList.map((data:any) => {
              return (
                <TaskList data={data} todoList={todoList} setTodoList={setTodoList}/>
              )
            })
          }
          </>
          :
          <></>
        }
      </ul>
    </div>
  </div>
  );
}

export default Home;