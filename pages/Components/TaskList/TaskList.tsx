// import './TaskList.css';
import React from 'react';

const TaskList = ({data, todoList, setTodoList}: any) => {

  const handleOnDelete = () => {
    const filteredData = todoList.filter(item => item.id !== data.id);
    setTodoList(filteredData);
  };

  return (
    <div className="taskListContainer">
      <div className="taskButtonContainer">
        <button className="taskButton" onClick={() => handleOnDelete()}>X</button>
      </div>
      <div className="taskListTitleContainer">
        <li className="taskListTitle">{data.task}</li>
      </div>
    </div>
  )
}

export default TaskList;